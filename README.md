<a class="mk-toclify" id="table-of-contents"></a>

# LIP - Mini Challenge on q/g jet tagging (taken from them IML workshop 2017 data challenge)
- [Introduction](#introduction)
- [Challenge Rules](#challenge-rules)
- [The simulation setup](#the-simulation-setup)
    - [Physics process and cuts](#physics-process-and-cuts)
    - [Detector setup](#detector-setup)
        - [Delphes FCC setup](#delphes-fcc-setup)
        - [Jet reconstruction](#jet-reconstruction)
- [The data format](#the-data-format)
    - [Location of the data sets in eos](#location-of-the-data-sets)
- [How to develop code for the challenge and examples](#how-to-develop-code-for-the-challenge-and-examples)
    - [The example and QA notebooks](#the-example-and-qa-notebooks)
        - [Basic QA Macro and Notebook (C++)](#basic-qa-macro-and-notebook-c)
        - [Python example: sklearn Naive Bayes](#python-example-sklearn-naive-bayes)
        - [Python example: Keras](#python-example-keras)
        - [ROOT TMVA example](#root-tmva-example)
- [Acknowledgements](#acknowledgements)
- [FAQ and Known Issues](#faq-and-known-issues)


<a class="mk-toclify" id="introduction"></a>
# Introduction

The purpose of this challenge is to develop and test methods to distinguish between quark and gluon jets.

The data sample has been generated simulating with Pythia 8 a Randal-Sundrum graviton, decaying either to a quark or to a gluon pair. 

The data have been then filtered using [Delphes](https://cp3.irmp.ucl.ac.be/projects/delphes). We only provide detector-level information for this challenge.

In order to simulate a more realistic situation (where a Monte Carlo simulation is never perfect), we provide 2 datasets, generated with different versions of Pythia:   Pythia 8.223 for the “standard" samples and 8.180 for the “modified" samples. This leads to a slightly different jet kinematics and substructure. 
The datasets are labelled as "Standard" and "Modified" below.

The training of your method should be based solely on the *Standard* data set, while the score which will be used for the challenge ranking should be based on the *Modified* one.
The idea is that in real-life you would train your method on Monte Carlo, and then apply it to real data (and in general there are small differences between data and MC).
Techniques to minimize the effect of these difference are therefore very welcome.

We realize that the few hours allocated for this challenge are not enough to develop any completely new method. At the same time, we believe that this is an excellent opportunity to compare apples-to-apples methods that you may have been already thinking about, or that are in use in your experimentsp

Finally, we also believe this also represents an excellent playground for people who are relatively new to machine learning. For instance, people who have just followed the tutorial can test they newly acquired skills on this data.

The provided recipes are intentionally very basic, as their main goal is to show how to access the data and provide a quick start for people new to machine learning.

We hope that you will enjoy this mini challenge!

<a class="mk-toclify" id="challenge-rules"></a>
# Challenge Rules

* The challenge lasts for the entire duration of the 2018 school on data sciences, organized by LIP
* It is allowed to form teams
* You can use any external computing resources you may have at your disposal
* The ranking will be based on the area under the ROC curve (AUC), estimated on the modified sample for a model trained on the standard sample
* Results should be presented by each team during the last hands-on session (Wed 14th)

<a class="mk-toclify" id="the-simulation-setup"></a>
# The simulation setup

<a class="mk-toclify" id="physics-process-and-cuts"></a>
## Physics process and cuts

The events contain Randall-Sundrum gravitons, inclusively produced from proton-proton collisions at sqrt(s)=13 TeV. The graviton mass is set to 200 GeV, while the kappa_G parameter is set to 0.00001, which results in a negligible width. The graviton is forced to decay to a pair of gluons or light quarks (up, down, or strange). As a reference, the Pythia setting follows for the gg case:

```
ExtraDimensionsG*:all  = on 
5100039:m0 = 200.  ! Mass (in GeV)
ExtraDimensionsG*:kappaMG = 0.00001 !TO SET WIDTH:  kappa m_G* = sqrt(2) x_1 k / Mbar_Pl, where x_1 = 3.83 
5100039:onMode = off  
5100039:onIfAny = 21  
```

and for the qq case:

```
ExtraDimensionsG*:all  = on 
5100039:m0 = 200.  ! Mass (in GeV)
ExtraDimensionsG*:kappaMG = 0.00001 !TO SET WIDTH:  kappa m_G* = sqrt(2) x_1 k / Mbar_Pl, where x_1 = 3.83 
5100039:onMode = off  ! switch off all decays
5100039:onIfAny = 1 2 3 ! switch on the decays to qq (q=u,d,s,c,b)
```

The pT distributions of the quark and gluon jets produced in this process are quite different. 
The classification algorithm should not use directly this difference, as this would defeat the spirit of the challenge. To limit the possibility to leverage on the different pt distribution, only reconstructed jets with pT between 100 and 130 GeV have been selected. We only provide jets which are originating from the initial graviton decay (jets from initial and final state radiation are not included in the sample).



<a class="mk-toclify" id="detector-setup"></a>
## Detector setup

<a class="mk-toclify" id="delphes-fcc-setup"></a>
### Delphes FCC setup 

The Delphes implementation of a possible FCC detector has been used for the simulation.
We used version 3.4.0.
The detector covers |eta| < 6 and includes a tracker and two calorimeters (hadronic and electromagnetic, with different granularities). The efficiency and the calorimeter granularity depend on eta. For more details, see the delphes card: [card](https://github.com/delphes/delphes/blob/3.4.0/cards/FCC/FCChh.tcl) or this [presentation](https://indico.cern.ch/event/550509/contributions/2413234/attachments/1395960/2128279/fccphysics_week_v4.pdf).



<a class="mk-toclify" id="jet-reconstruction"></a>
### Jet reconstruction ###

Jets have been reconstructed using fastjet, with the anti-kt algorithm and a R parameter of 0.4.
The particle-flow algorithm was used, therefore the constituents of the jet are both "tracks" and (calorimeter) "towers". 
As discussed above, there are an electromagnetic and hadronic calorimeters available. The data format (see below) allows you to access the EM and hadronic energy separately. 

<a class="mk-toclify" id="the-data-format"></a>
# The data format

The data are provided as a simple root tree format, with no external dependencies.
Each entry of the tree is a jet. The tree contains a few jet-level variables and an array of constituents (tracks and towers).

The examples discussed below show how to read this tree format using either pyroot or ROOT/C++.

This is the definition of the tree:
```C++
treeOut->Branch("jetPt"  ,&jetPt  , "jetPt/F");
treeOut->Branch("jetEta" ,&jetEta , "jetEta/F");
treeOut->Branch("jetPhi" ,&jetPhi , "jetPhi/F");
treeOut->Branch("jetMass",&jetMass, "jetMass/F");

treeOut->Branch("ntracks",&ntracks,"ntracks/I");
treeOut->Branch("ntowers",&ntowers,"ntowers/I");

treeOut->Branch("trackPt"     , trackPt    ,"trackPt[ntracks]/F");
treeOut->Branch("trackEta"    , trackEta   ,"trackEta[ntracks]/F");
treeOut->Branch("trackPhi"    , trackPhi   ,"trackPhi[ntracks]/F");
treeOut->Branch("trackCharge" , trackCharge,"trackCharge[ntracks]/F");
treeOut->Branch("towerE"      , towerE     ,"towerE[ntowers]/F");
treeOut->Branch("towerEem"    , towerEem   ,"towerEem[ntowers]/F");
treeOut->Branch("towerEhad"   , towerEhad  ,"towerEhad[ntowers]/F");
treeOut->Branch("towerEta"    , towerEta   ,"towerEta[ntowers]/F");
treeOut->Branch("towerPhi"    , towerPhi   ,"towerPhi[ntowers]/F");
```


<a class="mk-toclify" id="location-of-the-data-sets"></a>
## Location of the data sets

The datasets are available in [this link](https://cernbox.cern.ch/index.php/s/McixV1W54X5B5l0), which gives you access to the following directories:
```
quarks_standard
gluons_standard
quarks_modified
gluons_modified
```

The training of your method should be based solely on the Standard data set, while the score which will be used for the challenge ranking should be based on the Modified one. The idea is that in real-life you would train your method on Monte Carlo, and then apply it to real data (and in general there are small differences between data and MC). Techniques to minimize the effect of these difference are therefore very welcome.

<a class="mk-toclify" id="how-to-develop-code-for-the-challenge-and-examples"></a>
# How to develop code for the challenge and examples

We provide a few examples (in ROOT/C++ and python) which show you how to get started.

The methods which we implemented in the examples are simple and not computationally expensive. 


<a class="mk-toclify" id="the-example-and-qa-notebooks"></a>
## The example and QA notebooks ##

The discussion below contains links to jupyter notebooks on the gitlab repository. Unfortunately, gitlab does not properly preview jupyter notebooks, so you will see the (hugly) raw json format.

<a class="mk-toclify" id="basic-qa-macro-and-notebook-c"></a>
### Basic QA Macro and Notebook (C++) ###

In the [examples folder](https://gitlab.cern.ch/IML-WG/IMLWorkshop2017-Challenge/tree/master/Examples) you find a macro called [FilteredTreesQA.C](https://gitlab.cern.ch/IML-WG/IMLWorkshop2017-Challenge/blob/master/Examples/FilteredTreesQA.C) which shows how to read events from the quarks and gluons tree, and plots some basic distributions of jets and constituents as a simple QA. See the comment at the top of the macro, or have a look to the [TreeQA.pynb](https://gitlab.cern.ch/IML-WG/IMLWorkshop2017-Challenge/blob/master/Examples/TreeQA.ipynb) notebook for an example on how to use it.

<a class="mk-toclify" id="python-example-sklearn-naive-bayes"></a>
### Python example: sklearn Naive Bayes ###

The first training example uses a very simple naive (Gaussian) Bayesian method.

The classification is based on 4 jet shapes (high level features): mass, radial moment, the number of towers and the dispersion. For this simple example, only tracks are used to compute the radial moment and the dispersion.

The notebook also shows how to compute the area under the ROC curve (AUC) and save the corresponding value.

You can find the notebook [here](https://gitlab.cern.ch/IML-WG/IMLWorkshop2017-Challenge/blob/master/Examples/imlcoordinators_v0_NaiveBayes_main.ipynb) 
(Again, we suggest that you fork the project, clone it on your cernbox space and open the notebook on swan).


<a class="mk-toclify" id="python-example-keras"></a>
### Python example: Keras ###

The Keras example is very similar to the naive Bayes one. It is based on the same jet shapes, but a fully-connected feed-forward network is used for the classification.
The notebook is available [here](https://gitlab.cern.ch/IML-WG/IMLWorkshop2017-Challenge/blob/master/Examples/imlcoordinators_v1_Keras_main.ipynb)

<a class="mk-toclify" id="root-tmva-example"></a>
### ROOT TMVA example ###

The TMVA example is divided in various files, under this [folder](https://gitlab.cern.ch/IML-WG/IMLWorkshop2017-Challenge/tree/master/Examples/TMVA).

The notebook [`TMVAClassification.ipynb`](https://gitlab.cern.ch/IML-WG/IMLWorkshop2017-Challenge/tree/master/Examples/TMVA/TMVAClassification.ipynb) implements an example workflow for running MVA methods shipped with ROOT/TMVA. The method is then applied on the "modified" sample in the [TMVAMeasureAUC.ipynb](https://gitlab.cern.ch/IML-WG/IMLWorkshop2017-Challenge/blob/master/Examples/TMVA/TMVAMeasureAUC.ipynb)  notebook.

It is shipped with a couple of preprocessing script `PREPROCESS_DATA` and `PREPROCESS_DATA_MODIFIED`, which do a simple zero-padding strategy on the varying number of tracks and towers per event, so that we have a fix number of input variables in the preprocessed data. The comments in the notebooks will guide you through the process.


<a class="mk-toclify" id="acknowledgements"></a>
# Acknowledgements #

This data challenge was used during the IML 2017 workshop. LIP thanks the IML coordinators for the authorization of using it in the context of the 2018 LIP school on data sciences.

Original gitlab repository: https://gitlab.cern.ch/IML-WG/IMLWorkshop2017-Challenge

These are the original (IML 2017) acknowledgements:

We would like to thank Maurizio Pierini, Danilo Piparo, Enric Tejedor Saavedra, Rudiger Haake, Leticia Cunqueiro, Stefan Wunsch for their help in the preparation of the challenge and testing of the examples.

<a class="mk-toclify" id="faq-and-known-issues"></a>
# FAQ and Known Issues 

See [here](https://gitlab.cern.ch/IML-WG/IMLWorkshop2017-Challenge/blob/master/KnownIssues.md)

<!--  LocalWords:  graviton Delphes hadronic gluon
-->

